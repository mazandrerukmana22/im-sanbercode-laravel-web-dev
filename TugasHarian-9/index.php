<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$hewan = new Hewan("Kambing");
echo "Jenis Hewan : " . $hewan->jenis_hewan . "<br>";
echo "Nama Hewan : " . $hewan->nama . "<br>";
echo "Kaki Hewan : " . $hewan->legs . "<br>";
echo "Darah Hewan : " . $hewan->cold_blood . "<br> <br>";

$frog = new Frog("Katak");
echo "Jenis Hewan : " . $frog->jenis_hewan . "<br>";
echo "Nama Hewan : " . $frog->nama . "<br>";
echo "Kaki Hewan : " . $frog->legs . "<br>";
echo "Darah Hewan : " . $frog->cold_blood . "<br>";
echo $frog->jalan("HopHop");
echo "<br>";

$ape = new ape("Kera");
echo "Jenis Hewan : " . $ape->jenis_hewan . "<br>";
echo "Nama Hewan : " . $ape->nama . "<br>";
echo "Kaki Hewan : " . $ape->legs . "<br>";
echo "Darah Hewan : " . $ape->cold_blood . "<br>";
echo $frog->jalan("Auooo");
echo "<br>";