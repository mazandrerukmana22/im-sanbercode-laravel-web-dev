<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }
    public function kirim(Request $request)
    {
        $namadepan = $request->input('fnama');
        $namabelakang = $request->input('lnama');

        return view('page.Dashboard', ["namadepan" => $namadepan , "namabelakang" => $namabelakang]);
    }
}
