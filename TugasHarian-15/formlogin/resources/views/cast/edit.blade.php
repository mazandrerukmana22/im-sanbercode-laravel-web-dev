@extends('layouts.master')

@section('title')
    Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="mb-3">
      <label>Nama Cast</label>
      <input type="text" name="nama" value="{{$cast->name}}" class="@error('nama') is-invalid @enderror form-control">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message}}</div>
    @enderror
    <div class="mb-3">
      <div class="mb-3">
        <label>Umur</label>
        <input type="text" name="umur" value="{{$cast->umur}}class="@error('umur') is-invalid @enderror form-control">
      </div>
      @error('umur')
      <div class="alert alert-danger">{{ $message}}</div>
    @enderror
      <label>Bio</label>
      <textarea name="bio" value="{{$cast->bio}} class="@error('bio') is-invalid @enderror form-control" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection