<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'Home']);
Route::get('/register', [AuthController::class, 'register']);

Route::post('/kirim',[AuthController::class, 'kirim']);

/*test */
Route::get('/master', function(){
    return view('home');
});

Route::get('/data-tables', function(){
    return view('page.data-tables');
});
Route::get('/table', function(){
    return view('page.table');
});

//CRUD

//create
Route::get('/cast/create',[CastController::class,'create']);
Route::post('/cast',[CastController::class,'store']);

//read
Route::get('/cast',[CastController::class,'index']);
Route::get('/cast/{id}',[CastController::class,'show']);

//update
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
Route::get('/cast/{id}/edit',[CastController::class,'update']);

//delete
Route::delete('/cast/{id}',[CastController::class,'destroy']);
